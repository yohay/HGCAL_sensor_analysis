########################################################
# CMake file for HexPlot
CMAKE_MINIMUM_REQUIRED(VERSION 3.0 FATAL_ERROR)
IF(COMMAND CMAKE_POLICY)
  CMAKE_POLICY(SET CMP0003 NEW)
ENDIF(COMMAND CMAKE_POLICY)
########################################################

# Project name
PROJECT(HexPlot)

# Configure the installation prefix to allow both local as system-wide installation
IF(CMAKE_INSTALL_PREFIX_INITIALIZED_TO_DEFAULT)
    SET(CMAKE_INSTALL_PREFIX "${PROJECT_SOURCE_DIR}" CACHE PATH "Prefix prepended to install directories" FORCE)
ENDIF()

# Set up the RPATH so executables find the libraries even when installed in non-default location
SET(CMAKE_MACOSX_RPATH TRUE)
SET(CMAKE_SKIP_BUILD_RPATH FALSE)
SET(CMAKE_BUILD_WITH_INSTALL_RPATH FALSE)

# Add the automatically determined parts of the RPATH which point to directories outside the build tree to the install RPATH
SET(CMAKE_INSTALL_RPATH_USE_LINK_PATH TRUE)
# the RPATH to be used when installing, but only if it's not a system directory
LIST(FIND CMAKE_PLATFORM_IMPLICIT_LINK_DIRECTORIES "${CMAKE_INSTALL_PREFIX}/lib" isSystemDir)
IF("${isSystemDir}" STREQUAL "-1")
   SET(CMAKE_INSTALL_RPATH "${CMAKE_INSTALL_PREFIX}/lib")
ENDIF("${isSystemDir}" STREQUAL "-1")


##############################
# Prerequisistes for HexPlot #
##############################

INCLUDE("cmake/compiler-flag-checks.cmake")

# Check for C++17 and require C++17 if available else fallback to C++14 (should currently be sufficient)
CHECK_CXX_COMPILER_FLAG(-std=c++17 SUPPORT_STD_CXX17)
IF(SUPPORT_STD_CXX17)
    SET(CMAKE_CXX_STANDARD 17)
ELSE()
    SET(CMAKE_CXX_STANDARD 14)
ENDIF()
SET(CMAKE_CXX_STANDARD_REQUIRED ON)
SET(CMAKE_CXX_EXTENSIONS OFF)

# ROOT is required, component HistPainter is required for TPaletteAxis
FIND_PACKAGE(ROOT REQUIRED COMPONENTS HistPainter)
IF(NOT ROOT_FOUND)
    MESSAGE(FATAL_ERROR "Could not find ROOT, make sure to source the ROOT environment\n"
    "$ source YOUR_ROOT_DIR/bin/thisroot.sh")
ENDIF()

# Downgrade to C++14 if ROOT is not build with C++17 support
IF(ROOT_CXX_FLAGS MATCHES ".*std=c\\+\\+1[7z].*")
    IF(NOT SUPPORT_STD_CXX17)
        MESSAGE(FATAL_ERROR "ROOT was built with C++17 support but current compiler doesn't support it")
    ENDIF()
ELSEIF(ROOT_CXX_FLAGS MATCHES ".*std=c\\+\\+1[14y].*")
    SET(CMAKE_CXX_STANDARD 14)
ELSEIF(ROOT_CXX_FLAGS MATCHES ".*std=c\\+\\+.*")
    MESSAGE(FATAL_ERROR "ROOT was built with an unsupported C++ version: ${ROOT_CXX_FLAGS}")
ELSE()
    MESSAGE(FATAL_ERROR "Could not deduce ROOT's C++ version from build flags: ${ROOT_CXX_FLAGS}")
ENDIF()

###################################
# Load cpp format and check tools #
###################################

# Set the clang-format version required by the CI for correct formatting:
SET(CLANG_FORMAT_VERSION "8")

# Set the source files to clang-format (FIXME: determine this better)
FILE(GLOB_RECURSE
     CHECK_CXX_SOURCE_FILES
        src/*.[tch]xx src/*.h
     )
INCLUDE("cmake/clang-cpp-checks.cmake")

####################################
# Define build targets for HexPlot #
####################################

# Include directories
INCLUDE_DIRECTORIES(src)
INCLUDE_DIRECTORIES(SYSTEM ${ROOT_INCLUDE_DIRS})

# Add targets for Doxygen code reference
ADD_SUBDIRECTORY(doc)

# Add subdirectory with targets for libraries and executable
ADD_SUBDIRECTORY(src)

###################################
# Setup tests for HexPlot         #
###################################

# Enable testing
ENABLE_TESTING()

# Include all tests
ADD_SUBDIRECTORY(test)
