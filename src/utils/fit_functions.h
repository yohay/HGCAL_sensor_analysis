/**
        @file fit_functions.h
        @brief A collection of fit functions
  @author Andreas Alexander Maier
*/

#ifndef FIT_FUNCTIONS_H
#define FIT_FUNCTIONS_H

#include <TMath.h>

#include "cpp_utils.h"

///////////////////////////////////////////////////////////////////
// elementary functions
///////////////////////////////////////////////////////////////////
inline static Double_t mylandau(Double_t* x, Double_t* par) {  // NOLINT
  // par[0] ~ normalization, par[1] ~ mean, par[2] broadness
  return par[0] * (TMath::Landau(x[0], par[1], par[2]) / par[2]);
}
inline static Double_t invlandau(Double_t* x, Double_t* par) {  // NOLINT
  // par[0] ~ normalization, par[1] ~ mean, par[2] broadness
  return par[0] * (TMath::Landau(-x[0], par[1], par[2]) / par[2]);
}
inline static Double_t signal(Double_t* x, Double_t* par) {  // NOLINT
  // par[0] ~ normalization, par[1] ~ mean, par[2] width
  // return par[0]*exp(-0.5*pow((x[0]-par[1])/par[2],2));
  return par[0] * TMath::Gaus(x[0], par[1], par[2]);
}
inline static Double_t poisson(Double_t* x, Double_t* par) {  // NOLINT
  // par[0] ~ normalization, par[1] ~ max
  return par[0] * (TMath::Poisson(x[0], par[1]));
}
inline static Double_t linear(Double_t* x, Double_t* par) {  // NOLINT
  // printf("Returning %.2f + %.2f*%.2f = %.2f\n",par[0], x[0],par[1],par[0] + x[0]*par[1]);
  return par[0] + x[0] * par[1];
}
inline static Double_t quadratic(Double_t* x, Double_t* par) {  // NOLINT
  // printf("Returning %.2f + %.2f*%.2f = %.2f\n",par[0], x[0],par[1],par[0] + x[0]*par[1]);
  return par[0] + x[0] * par[1] + x[0] * x[0] * par[2];
}

inline static Double_t hinkley(Double_t* zz, Double_t* par) {  // NOLINT

  double z = zz[0];
  double norm = par[0];
  double mx = par[1];
  double sx = par[2];
  double my = par[3];
  double sy = par[4];   // par[3]
  double rho = par[5];  // par[4]
  double onemr2 = 1.0 - pow(rho, 2);
  double sonemr2 = sqrt(onemr2);
  double a = sqrt(pow(z / sx, 2) - 2.0 * rho * z / (sx * sy) + 1.0 / pow(sy, 2));
  double b = mx * z / pow(sx, 2) - rho * (mx + my * z) / (sx * sy) + my / pow(sy, 2);
  double c = pow(mx / sx, 2) - 2.0 * rho * mx * my / (sx * sy) + pow(my / sy, 2);
  double d = exp((pow(b, 2) - c * pow(a, 2)) / (2.0 * onemr2 * pow(a, 2)));
  double pi = TMath::Pi();
  double result = norm * (b * d / (pow(a, 3) * sqrt(2.0 * pi) * sx * sy) * TMath::Erf(b / (sonemr2 * a)) +
                          sonemr2 / (pow(a, 2) * pi * sx * sy) * exp(-c / (2.0 * onemr2)));
  return result;
}

inline static Double_t cryball(Double_t* z, Double_t* par) {  // NOLINT

  // Crystal Ball function
  // Gaussian: N = normalisation, m = mean, s = sigma
  // Tail:     a = boundary in sigma between gaussian and tail, n = relative hight of tail
  double x = -z[0];
  double N = par[0];
  double m = -par[1];
  double s = par[2];
  double a = par[3];
  double n = par[4];

  double A = pow(n / fabs(a), n) * exp(-pow(a, 2) / 2);
  double B = n / fabs(a) - fabs(a);

  double result = 0;
  if ((x - m) / s > -a) {
    result = N * exp(-pow(x - m, 2) / (2 * pow(s, 2)));
  } else {
    result = N * A * pow(B - (x - m) / s, -n);
  }

  return result;
}

inline static double Novosibirsk(Double_t* x, Double_t* par) {  // NOLINT
  double peak = par[0];
  double width = par[1];
  double tail = par[2];
  /*
  double sqrtln4 = sqrt(TMath::Log(4));
  double q = 1+((tail*(x[0]-peak)/sigma)*TMath::SinH(tail*sqrtln4)/tail*sqrtln4);
  double novo = TMath::Exp(pow(-0.5, pow(TMath::Log(q),2)/pow(tail,2)+pow(tail,2)));
  return novo;
  */
  // from http://root.cern.ch/root/html/src/RooNovosibirsk.cxx.html#ZTLfhD
  // see also : http://pprc.qmul.ac.uk/~bevan/afit/afit.pdf

  double qa = 0, qb = 0, qc = 0, qx = 0, qy = 0;

  if (TMath::Abs(tail) < 1.e-7) {
    qc = 0.5 * TMath::Power(((x[0] - peak) / width), 2);
  } else {
    qa = tail * sqrt(log(4.));
    qb = sinh(qa) / qa;
    qx = (x[0] - peak) / width * qb;
    qy = 1. + tail * qx;
    //---- Cutting curve from right side
    if (qy > 1.E-7) {
      qc = 0.5 * (TMath::Power((log(qy) / tail), 2) + tail * tail);
    } else {
      qc = 15.0;
    }
  }
  //---- Normalize the result
  return exp(-qc);
}
inline static double fit_function_asymmgauss(double* x, double* par)  // NOLINT
{
  double X = x[0];
  double Mean = par[1];
  double SigmaLeft = par[2];
  double SigmaRight = par[3];  // SigmaLeft*1.3; //
  double Sigma = (Mean < X) ? SigmaLeft : SigmaRight;
  return par[0] * TMath::Gaus(X, Mean, Sigma);
}
inline static Double_t gammadistr(Double_t* x, Double_t* par) {  // NOLINT
  // par[0] = normalization, par[1]=gamma - shape parameter, par[2]=mu - location parameter, par[3]=beta  - scale
  // parameter
  if (par[1] < 0 || x[0] < par[2] || par[3] < 0) {
    return 0;
  }
  { return par[0] * TMath::GammaDist(x[0], par[1], par[2], par[3]); }
}
inline static Double_t weibull(Double_t* x, Double_t* par) {  // NOLINT
  // par[0]=norm,par[1]=x-offset,par[2]=x-scaling,par[3]=form parameter
  double N = par[0];
  double xoffset = par[1];
  double xscale = par[2];
  double k = par[3];
  double X = (x[0] - xoffset) / xscale;
  double value = N * pow(X, k - 1) * exp(-pow(X, k));
  return value;
}
inline static Double_t rayleigh(Double_t* x, Double_t* par) {  // NOLINT
  // par[0]=norm,par[1]=x-offset,par[2]=x-scaling,par[3]=scaling parameter,par[4]=form parameter
  double X = (x[0] - par[1]) / par[2];
  double s = par[3];
  double value = X / pow(s, 2) * exp(-pow(X, 2) / (2 * pow(s, 2)));
  return par[0] * value;
}

///////////////////////////////////////////////////////////////////
// composite functions
///////////////////////////////////////////////////////////////////

inline static Double_t fit_function_langauss(Double_t* x, Double_t* par) {  // NOLINT
  double value = signal(x, par) + mylandau(x, &par[3]);
  return positive(value);
}
inline static Double_t fit_function_doublegauss(Double_t* x, Double_t* par) {  // NOLINT
  double value = signal(x, par) + signal(x, &par[3]);
  return positive(value);
}
inline static Double_t fit_function_hinklan(Double_t* x, Double_t* par) {  // NOLINT
  double value = hinkley(x, par) + mylandau(x, &par[6]);
  return positive(value);
}
inline static Double_t fit_function_poigauss(Double_t* x, Double_t* par) {  // NOLINT
  double value = signal(x, par) + poisson(x, &par[3]);
  return positive(value);
}
inline static Double_t fit_function_lanpoiss(Double_t* x, Double_t* par) {  // NOLINT
  double value = mylandau(x, par) + poisson(x, &par[3]);
  return positive(value);
}
inline static Double_t fit_function_hinkpoi(Double_t* x, Double_t* par) {  // NOLINT
  double value = hinkley(x, par) + poisson(x, &par[6]);
  return positive(value);
}
inline static Double_t fit_function_cryballgauss(Double_t* x, Double_t* par) {  // NOLINT
  double value = cryball(x, par) + signal(x, &par[5]);
  return positive(value);
}
inline static Double_t fit_function_cryballlandau(Double_t* x, Double_t* par) {  // NOLINT
  double value = cryball(x, par) + mylandau(x, &par[5]);
  return positive(value);
}
inline static Double_t fit_function_landoublegauss(Double_t* x, Double_t* par) {  // NOLINT
  // double val= mylandau(x,par) + signal(x,&par[3]) + signal(x,&par[6]);
  // retudrn val>=0?val:0;
  double value = mylandau(x, par) + signal(x, &par[3]) + signal(x, &par[6]);
  return positive(value);
}
inline static Double_t fit_function_triplegauss(Double_t* x, Double_t* par) {  // NOLINT
  double value = signal(x, par) + signal(x, &par[3]) + signal(x, &par[6]);
  return positive(value);
}
inline static Double_t fit_function_cryballlinear(Double_t* x, Double_t* par) {  // NOLINT
  double value = linear(x, par) + cryball(x, &par[2]);
  return positive(value);
}
inline static Double_t fit_function_lanasymmgauss(Double_t* x, Double_t* par) {  // NOLINT
  double value = mylandau(x, par) + fit_function_asymmgauss(x, &par[3]);
  return positive(value);
}
inline static Double_t fit_function_doublelandau(Double_t* x, Double_t* par) {  // NOLINT
  double value = mylandau(x, par) + mylandau(x, &par[3]);
  return positive(value);
}
inline static Double_t fit_function_gaussasymmgauss(Double_t* x, Double_t* par) {  // NOLINT
  double value = signal(x, par) + fit_function_asymmgauss(x, &par[3]);
  return positive(value);
}
inline static Double_t fit_function_invlangauss(Double_t* x, Double_t* par) {  // NOLINT
  double value = signal(x, par) + invlandau(x, &par[3]);
  return positive(value);
}
inline static Double_t fit_function_gammagauss(Double_t* x, Double_t* par) {  // NOLINT
  double value = gammadistr(x, par) + signal(x, &par[4]);
  return positive(value);
}
inline static Double_t fit_function_gammalandau(Double_t* x, Double_t* par) {  // NOLINT
  double value = gammadistr(x, par) + mylandau(x, &par[4]);
  return positive(value);
}
inline static Double_t fit_function_gammalinear(Double_t* x, Double_t* par) {  // NOLINT
  double value = gammadistr(x, par) + linear(x, &par[4]);
  return positive(value);
}
inline static Double_t fit_function_weibull(Double_t* x, Double_t* par) {  // NOLINT
  double value = weibull(x, par);
  return positive(value);
}
inline static Double_t fit_function_weibullgauss(Double_t* x, Double_t* par) {  // NOLINT
  double value = weibull(x, par) + signal(x, &par[4]);
  return positive(value);
}
inline static Double_t fit_function_invlandoublegauss(Double_t* x, Double_t* par) {  // NOLINT
  double value = invlandau(x, par) + signal(x, &par[3]) + signal(x, &par[6]);
  return positive(value);
}
inline static Double_t fit_function_linearintersection(Double_t* x, Double_t* par) {  // NOLINT
  // par[0] is the intersection point x0
  // function is a + b*x when x < x0
  // and c + d*x when x > x0
  // par[] contains [x0, a, b, d]
  // c is calculated as (a + b*x0) - d*x0
  auto* par2 = new double[2];
  par2[0] = linear(par, &par[1]) - par[3] * par[0];
  par2[1] = par[3];
  double value = x[0] < par[0] ? linear(x, &par[1]) : linear(x, par2);
  return value;
}

inline static Double_t smoothed_linearintersection(Double_t* x, Double_t* par) {  // NOLINT
  // same as linearintersection but with an extra parameter par[4] that controls smoothing
  auto* par2 = new double[2];
  par2[0] = linear(par, &par[1]) - par[3] * par[0];
  par2[1] = par[3];

  double k = par[4];
  double y0 = linear(x, &par[1]);
  double y1 = linear(x, par2);

  // define a smoothed minimum function, becomes a true minimum as k -> 0
  double value = std::pow(std::pow(std::abs(y0), -k) + std::pow(std::abs(y1), -k), -1 / k);
  value = std::copysign(value, y0);  // deal with y0 becoming negative, assume y1 cannot be
  return value;
}

inline static Double_t fit_function_quadlinearintersection(Double_t* x, Double_t* par) {  // NOLINT
  // par[0] is the intersection point x0
  // function is a + b*x - c*x*x when x < x0
  // and d + e*x when x > x0
  // par[] contains [x0, a, b, c, e]
  // d is calculated as (a + b*x0 - c*x0*x0) - e*x0
  auto* par2 = new double[2];
  par2[0] = (par[1] + par[0] * par[1] - par[0] * par[0] * par[2]) - par[4] * par[0];
  par2[1] = par[4];
  double value = x[0] < par[0] ? (par[1] + x[0] * par[1] - x[0] * x[0] * par[2]) : linear(x, par2);
  return value;
}

#endif
