/**
    @file example_handler.h
    @brief Handles examples of usage
    @author Andreas Alexander Maier
*/

#ifndef EXAMPLE_HANDLER_H
#define EXAMPLE_HANDLER_H

#include <utility>

#include "../utils/cpp_utils.h"

/// A class for handling example commands
class example_handler {
 public:
  /// Print the list of examples
  void print_examples();
  /// Return the ith example command
  std::string get_example_description(int i) { return descriptions.at(i); };
  std::string get_example(int i) { return mainCommand + commands.at(i); };
  /// Return the ith example command without main command
  std::string get_example_no_main_cmd(int i) { return commands.at(i); };
  /// Return number of examples
  int get_n_examples() { return nExamples; };
  /// Constructor
  explicit example_handler(const std::string& mainCMD);

 private:
  int nExamples;
  std::string mainCommand;
  std::string sourceDir;
  std::vector<std::string> commands;
  std::vector<std::string> descriptions;
  /// Adds an example
  void add_example(const std::string& descr, std::string options) {
    ++nExamples;
    descriptions.push_back(descr);
    options = replace_string_all(options, "../", sourceDir);
    commands.push_back(options);
  };
  /// Adds an example, using the description of the previous example
  void add_example(std::string options) { add_example(descriptions.back(), std::move(options)); };
};

#endif
