/**
        @file hex_map.h
        @brief Reads map files
  @author Andreas Alexander Maier
*/

#ifndef HEX_MAP_H
#define HEX_MAP_H

#include <utility>

#include "TTree.h"

#include "../utils/root_utils.h"
#include "globals.h"

/// Applies an optional mapping between the cell number in the data file and the cell number in the geometry file.
class hex_map {
 public:
  /**
  \brief Returns the mapped cell number

  Returns the cell number in the geometry file corresponding to the cell
  number in the data file, if an optional mapping is provided. Default
  is identity.
  */
  int geo_file_number(int iDataFile);
  /// Same for double arguments
  int geo_file_number(double iDataFile) { return geo_file_number((int(iDataFile))); };
  /// Constructur
  explicit hex_map(std::string mapFile, int verbose = 0);

 private:
  bool mapsLoaded;
  int geoFileNumbers[MAX_CELL_NUMBER];
  std::string mapFile;
  int verbose;

  /// Generates a mapping from the data file number to the cell number
  void load_maps();
};

#endif
