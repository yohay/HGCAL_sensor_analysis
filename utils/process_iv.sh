#!/bin/sh

source ~/.profile

LIST=(
	freesample_2
	freesample_3
	freesample_4
	freesample_5
	freesample_6
)

DIR="/Users/Home/Cloud/Cernbox/hgcSensorTesting/Results"
TYPE="HPK_8in_256"
GEO="HPK_256ch_8inch.txt"

# IV Loop
for ID in "${LIST[@]}"
do
	echo "Correcting data file ..."
	python ./utils/correct_iv.py -i ${DIR}/${TYPE}_${ID}/${TYPE}_${ID}_IV.txt --cor 1 --inv 1
	echo "Creating plot folder ..."
	PLOTDIR=${DIR}/${TYPE}_${ID}/detailed_plots
	mkdir ${PLOTDIR}
	echo "Plotting data ..."
	./bin/HexPlot -i ${DIR}/${TYPE}_${ID}/${TYPE}_${ID}_IV_corrected.txt -g ./geo/hex_positions_${GEO} -o ${PLOTDIR}/${TYPE}_${ID}_IV_corrected.pdf --IV --detailed
	./bin/HexPlot -i ${DIR}/${TYPE}_${ID}/${TYPE}_${ID}_IV_corrected.txt -g ./geo/hex_positions_${GEO} -o ${DIR}/${TYPE}_${ID}/${TYPE}_${ID}_IV_corrected.pdf --IV -z 0:80
	./bin/HexPlot -i ${DIR}/${TYPE}_${ID}/${TYPE}_${ID}_IV_corrected.txt -g ./geo/hex_positions_${GEO} -o ${DIR}/${TYPE}_${ID}/${TYPE}_${ID}_IV_corrected_FULL.pdf --IV -p FULL
	echo "\n\n"
done
