#!/usr/bin/python
from __future__ import print_function

import os
import math

print('Converting geo files to new syntax...')

CONVERT_ROTATION = False
CONVERT_SIZE = False
CONVERT_YPOS = False
CONVERT_SIZE_POSITION = True
CONVERT_X_Y_PRECISION = True
RESIZE_TYPES = [0, 2, 3, 4, 5, 6, 10, 11, 21, 22, 23]

GEO_DIR = '../geo/'
TMP_FILE = 'tmp.txt'
TMP_FILE2 = 'tmp2.txt'

for geoFile in os.listdir(GEO_DIR):

    if geoFile == 'example_shapes.txt':
        continue

    print('Converting', geoFile)

    with open(GEO_DIR + geoFile) as origFile:
        with open(TMP_FILE, 'w') as tmpFile:

            for line in origFile:
                line = line.strip()
                if 'padNumber' in line:
                    print('# padNumber	xPosition	yPosition  type    optional', file=tmpFile)
                    continue
                if line.startswith('#') or not line:
                    print(line, file=tmpFile)
                    continue
                else:
                    splitLine = line.split()
                    try:
                        doResize = (int(splitLine[4]) in RESIZE_TYPES)
                    except ValueError:
                        doResize = False
                    for index, word in enumerate(splitLine):
                        if CONVERT_ROTATION and index == len(splitLine) - 1:
                            if word == '0':
                                print('STD', end='', file=tmpFile)
                            else:
                                print('ROTATE:' + word, end='', file=tmpFile)
                            continue
                        if index == 3:
                            newWord = str(float(word) * 2)
                            if CONVERT_SIZE and doResize:
                                print(newWord, end='\t', file=tmpFile)
                                continue
                        if CONVERT_YPOS and index == 2:
                            newWord = str(float(word) / (2. / math.sqrt(3.)))
                            print(newWord, end='\t', file=tmpFile)
                            continue
                        print(word, end='\t', file=tmpFile)
                    print('', file=tmpFile)

    with open(TMP_FILE) as origFile:
        with open(TMP_FILE2, 'w') as tmpFile:
            for line in origFile:
                line = line.strip()
                if 'padNumber' in line:
                    print('# padNumber	xPosition	yPosition  type    optional', file=tmpFile)
                    continue
                if line.startswith('#') or not line:
                    print(line, file=tmpFile)
                    continue
                else:
                    splitLine = line.split()
                    savedSize = ''
                    for index, word in enumerate(splitLine):
                        if CONVERT_X_Y_PRECISION:
                            if index == 1 or index == 2:
                                word = "{0:+.2f}".format(float(word))
                        if index == 3:
                            savedSize = word
                            continue
                        if index == 5 and CONVERT_SIZE_POSITION:
                            if word == 'STD':
                                word = ''
                            else:
                                word = ',' + word
                            word = 'SIZE:' + savedSize + word
                        print(word, end='\t', file=tmpFile)
                    print('', file=tmpFile)

    os.remove(GEO_DIR + geoFile)
    os.remove(TMP_FILE)
    os.rename(TMP_FILE2, GEO_DIR + geoFile)
