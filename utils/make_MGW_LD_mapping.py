#author: Thorben Quast


from operator import ge
import numpy as np
import os, sys
from subprocess import Popen

#read data file provided by Ron Lipton
SCALE = 0.675         #need to scale all length indications from Ron to fit hexplot canvas dimensions
cell_types, xy_positions = np.genfromtxt("LD_Cells_1.txt", unpack=True, delimiter="  ", dtype=str)
x = np.array([_e.replace("(", "").replace(")", "").split(",")[0] for _e in xy_positions]).astype(np.float)
y = np.array([_e.replace("(", "").replace(")", "").split(",")[1] for _e in xy_positions]).astype(np.float)

#compute size in cm
x = x / 1E4 * SCALE
y = -y / 1E4 * SCALE

geo_positions = np.array([_ for _ in zip(cell_types, x, y)], dtype=[("type", np.object), ("x_cm", float), ("y_cm", float)])
geo_positions.sort(order=["y_cm", "x_cm", "type"])
geo_positions["y_cm"] = -geo_positions["y_cm"]

_type_ids = {
    'Pad': 0,
    'Pad_10e': 41,
    'Pad_10e_11e': 42,
    'Pad_10e_Cut': 43,
    'Pad_11e_12e': 11,
    'Pad_12e': 11,
    'Pad_12e_1e_3i': 11,
    'Pad_12e_Cut': 44,
    'Pad_12i': 11,
    'Pad_2e': 41,
    'Pad_2e_Cut': 43,
    'Pad_3i': 0,
    'Pad_3i_Half': 0,
    'Pad_3i_6e': 41,
    'Pad_3i_6i': 41,
    'Pad_4e': 11,
    'Pad_4e_Cut': 44,
    'Pad_5e_6e': 42,
    'Pad_6e': 41,
    'Pad_6e_7e': 42,
    'Pad_6e_9i': 41,
    'Pad_6e_Cut': 43,
    'Pad_6i': 41,
    'Pad_6i_9i': 41,
    'Pad_6i_wCalibration': 45,      #new calibration pad
    'Pad_7e_8e': 11,
    'Pad_8e': 11,
    'Pad_8e_9e_12i': 11,
    'Pad_8e_Cut': 44,
    'Pad_9i': 0,
    'Pad_9i_12e': 11,
    'Pad_9i_12i': 11,
    'Pad_9i_Half': 0,
    'Pad_wCalibration': 4,
    'Pad_12e_3i': 11,
    'Pad_3i_5e': 11,
    'Pad_12i_3i': 11,
    'Pad_12i_3e_4e': 11,
    'Pad_6i_9e_10e': 46,                
    'Pad_2e_3e_6i': 46,                  

    'Pad_9i_1e_2e_Cut': 47,
    'Pad_4e_5e_9i': 47,                 #to be implemented
}

_optionals = {
    'Pad': "SIZE:0.85,ROTATE:0",
    'Pad_10e': "SIZE:0.85,ROTATE:60",
    'Pad_10e_11e': "SIZE:0.85,ROTATE:60",
    'Pad_10e_Cut': "SIZE:0.85,ROTATE:60",
    'Pad_11e_12e': "SIZE:0.85,ROTATE:-120,X_CROP:0.5,INVERT:1",
    'Pad_12e': "SIZE:0.85,ROTATE:0",
    'Pad_12e_1e_3i': "SIZE:0.85,ROTATE:-120,X_CROP:0.5,ROTATE:120",
    'Pad_12e_Cut': "SIZE:0.85,ROTATE:0",
    'Pad_12i': "SIZE:0.85,ROTATE:0",
    'Pad_2e': " SIZE:0.85,ROTATE:300",
    'Pad_2e_Cut': "SIZE:0.85,ROTATE:300",
    'Pad_3i': "SIZE:0.85,X_CROP:0.05",
    'Pad_3i_Half': "SIZE:0.85,X_CROP:0.57",
    'Pad_3i_6e': "SIZE:0.85,ROTATE:180,X_CROP:0.57",
    'Pad_3i_6i': "SIZE:0.85,ROTATE:180,X_CROP:0.57",
    'Pad_4e': "SIZE:0.85,ROTATE:-120",
    'Pad_4e_Cut': "SIZE:0.85,ROTATE:-120",
    'Pad_5e_6e': "SIZE:0.85,ROTATE:-180,INVERT:1",
    'Pad_6e': "SIZE:0.85,ROTATE:180",
    'Pad_6e_7e': "SIZE:0.85,ROTATE:180",
    'Pad_6e_9i': "SIZE:0.85,ROTATE:-180,X_CROP:0.57,INVERT:1",
    'Pad_6e_Cut': "SIZE:0.85,ROTATE:180",
    'Pad_6i': "SIZE:0.85,ROTATE:180",
    'Pad_6i_9i': "SIZE:0.85,ROTATE:-180,X_CROP:0.57,INVERT:1",
    'Pad_6i_wCalibration': "SIZE:0.85,ROTATE:180,TEXT_Y_OFFSET:+0.27",
    'Pad_7e_8e': "SIZE:0.85,ROTATE:-120,X_CROP:0.5,ROTATE:-60",
    'Pad_8e': "SIZE:0.85,ROTATE:120",
    'Pad_8e_9e_12i': "SIZE:0.85,ROTATE:120,X_CROP:0.5,ROTATE:-120",
    'Pad_8e_Cut': " SIZE:0.85,ROTATE:120,TEXT_X_OFFSET:+0.1",
    'Pad_9i': "SIZE:0.85,X_CROP:0.05,ROTATE:180",
    'Pad_9i_12e': "SIZE:0.85,ROTATE:0,ROTATE:-60,X_CROP:0.025,INVERT:1,ROTATE:60",
    'Pad_9i_12i': "SIZE:0.85,ROTATE:0,ROTATE:-60,X_CROP:0.025,INVERT:1,ROTATE:60",
    'Pad_9i_Half': "SIZE:0.85,X_CROP:0.57,ROTATE:180",
    'Pad_wCalibration': "SIZE:0.85",
    'Pad_12e_3i': "SIZE:0.85,ROTATE:-60,X_CROP:0.05,ROTATE:60",
    'Pad_3i_5e': "SIZE:0.85,ROTATE:-120,X_CROP:0.5,ROTATE:-60,X_CROP:0.55,ROTATE:60",
    'Pad_12i_3i': "SIZE:0.85,ROTATE:-60,X_CROP:0.05,ROTATE:60",
    'Pad_12i_3e_4e': "SIZE:0.85,ROTATE:-60,X_CROP:0.5,ROTATE:+60",
    'Pad_6i_9e_10e': "SIZE:0.85,ROTATE:210,INVERT:1,ROTATE:30",
    'Pad_2e_3e_6i': "SIZE:0.85,ROTATE:180",
    'Pad_9i_1e_2e_Cut': "SIZE:0.85",
    'Pad_4e_5e_9i': "SIZE:0.85,ROTATE:210,INVERT:1,ROTATE:60,X_CROP:0.2,ROTATE:-30,ROTATE:45,X_CROP:0.25,ROTATE:-45"
}


calib_cell_counter = 0
OUTERCALIBPADS = [12, 63, 87, 145, 155, 163]
geo_file_path = "MGW_LD_2022.txt"
with open(geo_file_path, "w") as geo_file:
    geo_file.write("Thorben Quast, 15 Feb 2022 \n")
    geo_file.write("# padnumber     xposition       yposition       type    optional \n") 
    geo_file.write("\n\n\n")

    for _index in range(len(geo_positions)):
        index_in_list = _index + 1
        pad_nr = index_in_list + calib_cell_counter
        x_cm = geo_positions["x_cm"][_index]
        y_cm = geo_positions["y_cm"][_index]
        cell_type = geo_positions["type"][_index]
        
        _type_id = _type_ids[cell_type]
        _optional = _optionals[cell_type]

        if (_type_id is None) or (_optional is None):
            continue
        
        _entry = "%i    %.2f     %.2f      %i       %s" % (pad_nr, x_cm, y_cm, _type_id, _optional)
        geo_file.write("%s \n" % _entry)

        if index_in_list in OUTERCALIBPADS: 
            calib_cell_counter += 1
            pad_nr += 1
            _type_id = 0
            _optional = "SIZE:0.35,ROTATE:0"            
            _entry = "%i    %.2f     %.2f      %i       %s" % (pad_nr, x_cm, y_cm, _type_id, _optional)
            geo_file.write("%s \n" % _entry)
    
    #guard rings, pad numbers are not written but moved outside the field of view
    geo_file.write("213     +0.00   +0.00  1       SIZE:13.5,ROTATE:30,WIDTH:0.01,EDGE_SHIFT:0.6,TEXT_Y_OFFSET:+999\n")
    
    #test structures
    geo_file.write("214     +5.50   -3.90   12      SIZE:0.5\n")
    geo_file.write("215     +5.50   -4.33   12      SIZE:0.5\n")
    geo_file.write("216     +5.50   -4.76   12      SIZE:0.5\n")
    geo_file.write("217     +5.50   -5.20   12      SIZE:0.5\n")
    geo_file.write("218     +6.00   -3.90   12      SIZE:0.5\n")
    geo_file.write("219     +6.00   -4.33   12      SIZE:0.5\n")
    geo_file.write("220     +6.00   -4.76   12      SIZE:0.5\n")
    geo_file.write("221     +6.00   -5.20   12      SIZE:0.5\n")


#hexplot plotting
HEXPLOTCOMMAND = "<HEXPLOTDIR>/bin/HexPlot \
	    -g <GEOFILE> \
	    -o <OUTPUTFILE> \
	    -p GEO --pn 0".replace("<HEXPLOTDIR>", os.environ["HEXPLOT_DIR"]).replace("<GEOFILE>", geo_file_path).replace("<OUTPUTFILE>", geo_file_path.replace(".txt", ".pdf"))


env = {}
env.update(os.environ)
p = Popen(HEXPLOTCOMMAND, stdout=sys.stdout, stderr=sys.stderr, shell=True, env=env)
out, err = p.communicate()
code = p.returncode
