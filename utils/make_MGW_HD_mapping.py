#author: Thorben Quast


from operator import ge
import numpy as np
import os, sys
from subprocess import Popen

#read data file provided by Ron Lipton
SCALE = 0.675         #need to scale all length indications from Ron to fit hexplot canvas dimensions
cell_types, xy_positions = np.genfromtxt("HD_Cells.txt", unpack=True, delimiter="  ", dtype=str)
x = np.array([_e.replace("(", "").replace(")", "").split(",")[0] for _e in xy_positions]).astype(np.float)
y = np.array([_e.replace("(", "").replace(")", "").split(",")[1] for _e in xy_positions]).astype(np.float)

#compute size in cm
x = x / 1E4 * SCALE
y = -y / 1E4 * SCALE

geo_positions = np.array([_ for _ in zip(cell_types, x, y)], dtype=[("type", np.object), ("x_cm", float), ("y_cm", float)])
geo_positions.sort(order=["y_cm", "x_cm", "type"])
geo_positions["y_cm"] = -geo_positions["y_cm"]

_type_ids = {
    'Pad': 0,
    'Pad_10e': 41,
    'Pad_10e_11e': 42,
    'Pad_10e_12i': 41,         
    'Pad_10e_Cut': 43,
    'Pad_11e': 11,
    'Pad_11e_12e': 11,
    'Pad_12e': 11,
    'Pad_12e_1e': 11,
    'Pad_12e_3i_Left': 11,          
    'Pad_12e_3i_Right': 11,
    'Pad_12e_Cut': 44,
    'Pad_12i': 41,
    'Pad_12i_2e': 41,
    'Pad_12i_3i_Left': 41,
    'Pad_12i_3i_Right': 41,
    'Pad_1e': 11,
    'Pad_1e_2e': 42,
    'Pad_2e': 41,
    'Pad_2e_3e': 11,
    'Pad_2e_6i': 46,
    'Pad_2e_Cut': 43,
    'Pad_3e': 11,
    'Pad_3e_4e': 11,
    'Pad_3i_6e_Left': 41,
    'Pad_3i_6e_Right': 41,
    'Pad_3i_6i_Left': 11,
    'Pad_3i_6i_Right': 11,
    'Pad_3i_Left': 0,
    'Pad_3i_Left_Half': 0,                      #e.g. pad 32          
    'Pad_3i_Right': 0,
    'Pad_3i_Right_Half': 0,                    #e.g. pad 41
    'Pad_4e': 11,
    'Pad_4e_5e': 11,
    'Pad_4e_Cut': 44,
    'Pad_5e': 11,
    'Pad_5e_6e': 42,
    'Pad_6e': 41,
    'Pad_6e_7e': 42,
    'Pad_6e_9i_Left': 41,
    'Pad_6e_9i_Right': 41,
    'Pad_6e_Cut': 43,
    'Pad_6i': 11,
    'Pad_6i_10e': 46,
    'Pad_6i_9i_Left': 11,
    'Pad_6i_9i_Right': 11,
    'Pad_7e': 11,
    'Pad_7e_8e': 11,
    'Pad_8e': 11,
    'Pad_8e_9e': 11,
    'Pad_8e_Cut': 44,
    'Pad_9e': 11,
    'Pad_9e_10e': 11,
    'Pad_9i_12e_Left': 11,      
    'Pad_9i_12e_Right': 11,
    'Pad_9i_12i_Left': 41,
    'Pad_9i_12i_Right': 41,
    'Pad_9i_Left': 0,
    'Pad_9i_Left_Half': 0,
    'Pad_9i_Right': 0,
    'Pad_9i_Right_Half': 0,
    'Pad_9i_wCalibration': 4,
    'Pad_wCalibration': 4
}

_optionals = {
    'Pad': "SIZE:0.58,ROTATE:0",
    'Pad_10e': "SIZE:0.58,ROTATE:60",
    'Pad_10e_11e': "SIZE:0.58,ROTATE:60,ROTATE:-120,X_CROP:0.2,ROTATE:120",             #e.g. pad 15
    'Pad_10e_12i': "SIZE:0.58,ROTATE:-150,X_CROP:0.28,ROTATE:150", 
    'Pad_10e_Cut': "SIZE:0.58,ROTATE:60",
    'Pad_11e': "SIZE:0.58,ROTATE:-120,X_CROP:0.7,INVERT:1",                         #e.g. pad 1
    'Pad_11e_12e': "SIZE:0.58,ROTATE:-180,X_CROP:0.2,ROTATE:180",                   #e.g. pad 2
    'Pad_12e': "SIZE:0.58",
    'Pad_12e_1e': "SIZE:0.58,ROTATE:180,X_CROP:0.2,ROTATE:180,ROTATE:-60,INVERT:1,ROTATE:60",
    'Pad_12e_3i_Left': "SIZE:0.58,ROTATE:-60,X_CROP:0.71,ROTATE:60",                #e.g. pad4
    'Pad_12e_3i_Right': "SIZE:0.58,ROTATE:-60,X_CROP:0.71,ROTATE:60",
    'Pad_12e_Cut': "SIZE:0.58",
    'Pad_12i': "SIZE:0.58",
    'Pad_12i_2e': "SIZE:0.58,ROTATE:-30,X_CROP:0.28,ROTATE:30",
    'Pad_12i_3i_Left': "SIZE:0.58,X_CROP:0.71",        #e.g. pad 183
    'Pad_12i_3i_Right': "SIZE:0.58,X_CROP:0.71",
    'Pad_1e': "SIZE:0.58,ROTATE:-120,X_CROP:0.7,INVERT:1,ROTATE:-60",
    'Pad_1e_2e': "SIZE:0.58,ROTATE:60,ROTATE:-120,X_CROP:0.2,ROTATE:120,INVERT:1",
    'Pad_2e': "SIZE:0.58,ROTATE:300",
    'Pad_2e_3e': "SIZE:0.58,ROTATE:-180,X_CROP:0.2,INVERT:1,ROTATE:240",
    'Pad_2e_6i': "SIZE:0.58,ROTATE:180,ROTATE:90,X_CROP:0.175,ROTATE:-90",                       #e.g. pad180
    'Pad_2e_Cut': "SIZE:0.58,ROTATE:300",
    'Pad_3e': "SIZE:0.58,ROTATE:-120,X_CROP:0.7,INVERT:1,ROTATE:240",
    'Pad_3e_4e': "SIZE:0.58,ROTATE:-180,X_CROP:0.2,ROTATE:180,ROTATE:-120",             #e.g. pad278
    'Pad_3i_6e_Left': "SIZE:0.58,ROTATE:180,X_CROP:0.15",
    'Pad_3i_6e_Right': "SIZE:0.58,ROTATE:180,X_CROP:0.15",
    'Pad_3i_6i_Left': "SIZE:0.58,ROTATE:120,X_CROP:0.15,ROTATE:60",       #e.g. pad 161
    'Pad_3i_6i_Right': "SIZE:0.58,ROTATE:120,X_CROP:0.15,ROTATE:60",
    'Pad_3i_Left': "SIZE:0.58,X_CROP:0.15",
    'Pad_3i_Left_Half': "SIZE:0.58,X_CROP:0.71",
    'Pad_3i_Right': "SIZE:0.58,X_CROP:0.15",
    'Pad_3i_Right_Half': "SIZE:0.58,X_CROP:0.71",
    'Pad_4e': "SIZE:0.58,ROTATE:-120",
    'Pad_4e_5e': "SIZE:0.58,ROTATE:-180,X_CROP:0.2,ROTATE:60,INVERT:1,ROTATE:-120",         #e.g. pad 455
    'Pad_4e_Cut': "SIZE:0.58,ROTATE:-120",
    'Pad_5e': "SIZE:0.58,ROTATE:-120,X_CROP:0.7,INVERT:1,ROTATE:180",                   #e.g. pad 468
    'Pad_5e_6e': "SIZE:0.58,ROTATE:60,ROTATE:-120,X_CROP:0.2,ROTATE:120,INVERT:1,ROTATE:-120",
    'Pad_6e': "SIZE:0.58,ROTATE:180",
    'Pad_6e_7e': "SIZE:0.58,ROTATE:60,ROTATE:-120,X_CROP:0.2,ROTATE:120,ROTATE:120",
    'Pad_6e_9i_Left': "SIZE:0.58,ROTATE:180,X_CROP:0.15,INVERT:1",
    'Pad_6e_9i_Right': "SIZE:0.58,ROTATE:180,X_CROP:0.15,INVERT:1",
    'Pad_6e_Cut': "SIZE:0.58,ROTATE:180",
    'Pad_6i': "SIZE:0.58,ROTATE:180",
    'Pad_6i_10e': "SIZE:0.58,ROTATE:180,ROTATE:90,X_CROP:0.175,ROTATE:-90,INVERT:1",
    'Pad_6i_9i_Left': "SIZE:0.58,ROTATE:120,X_CROP:0.15,ROTATE:60,ROTATE:30,INVERT:1,ROTATE:-210",          #e.g. 162
    'Pad_6i_9i_Right': "SIZE:0.58,ROTATE:120,X_CROP:0.15,INVERT:1,ROTATE:60",               #e.g. pad 169
    'Pad_7e': "SIZE:0.58,ROTATE:-120,X_CROP:0.7,ROTATE:-60",                                           #e.g. pad 444
    'Pad_7e_8e': "SIZE:0.58,ROTATE:-180,X_CROP:0.2,ROTATE:300",
    'Pad_8e': "SIZE:0.58,ROTATE:120",
    'Pad_8e_9e': "SIZE:0.58,ROTATE:-180,X_CROP:0.2,ROTATE:180,ROTATE:-120,ROTATE:-30,INVERT:1,ROTATE:90",
    'Pad_8e_Cut': "SIZE:0.58,ROTATE:120",
    'Pad_9e': "SIZE:0.58,ROTATE:-120,X_CROP:0.7,INVERT:1,ROTATE:60",
    'Pad_9e_10e': "SIZE:0.58,ROTATE:-180,X_CROP:0.2,ROTATE:-120",
    'Pad_9i_12e_Left': "SIZE:0.58,ROTATE:-240,X_CROP:0.71,ROTATE:240",                          #e.g. pad4
    'Pad_9i_12e_Right': "SIZE:0.58,ROTATE:-240,X_CROP:0.71,ROTATE:240",
    'Pad_9i_12i_Left': "SIZE:0.58,X_CROP:0.71,INVERT:1",
    'Pad_9i_12i_Right': "SIZE:0.58,X_CROP:0.71,INVERT:1",
    'Pad_9i_Left': "SIZE:0.58,X_CROP:0.15,INVERT:1",
    'Pad_9i_Left_Half': "SIZE:0.58,X_CROP:0.71,ROTATE:180",
    'Pad_9i_Right': "SIZE:0.58,X_CROP:0.15,INVERT:1",
    'Pad_9i_Right_Half': "SIZE:0.58,X_CROP:0.71,ROTATE:180",
    'Pad_9i_wCalibration': "SIZE:0.58,X_CROP:0.15,INVERT:1",
    'Pad_wCalibration': "SIZE:0.58"    
}


calib_cell_counter = 0
OUTERCALIBPADS = [30, 37, 111, 135, 141, 215, 246, 298, 326, 375, 415, 421]
missing_types = []
geo_file_path = "MGW_HD_2022.txt"
with open(geo_file_path, "w") as geo_file:
    geo_file.write("Thorben Quast, 17 Feb 2022 \n")
    geo_file.write("# padnumber     xposition       yposition       type    optional \n") 
    geo_file.write("\n\n\n")

    for _index in range(len(geo_positions)):
        index_in_list = _index + 1
        pad_nr = index_in_list + calib_cell_counter
        x_cm = geo_positions["x_cm"][_index]
        y_cm = geo_positions["y_cm"][_index]
        cell_type = geo_positions["type"][_index]
        if pad_nr == 2:
            print(pad_nr,cell_type)
        if pad_nr == 278:
            print(pad_nr,cell_type)
        if not cell_type in _type_ids:
            missing_types.append(cell_type)
            continue        
        _type_id = _type_ids[cell_type]
        _optional = _optionals[cell_type]

        if (_type_id is None) or (_optional is None):
            continue
        
        _entry = "%i    %.2f     %.2f      %i       %s" % (pad_nr, x_cm, y_cm, _type_id, _optional)
        geo_file.write("%s \n" % _entry)

        if index_in_list in OUTERCALIBPADS: 
            calib_cell_counter += 1
            pad_nr += 1
            _type_id = 0
            _optional = "SIZE:0.20,ROTATE:0"            
            _entry = "%i    %.2f     %.2f      %i       %s" % (pad_nr, x_cm, y_cm, _type_id, _optional)
            geo_file.write("%s \n" % _entry)
    
    #guard rings, pad numbers are not written but moved outside the field of view
    geo_file.write("469     +0.00   +0.00  1       SIZE:13.5,ROTATE:30,WIDTH:0.01,EDGE_SHIFT:0.6,X_CROP:0.6425,Y_CROP:0.5875,INVERT:1,ROTATE:180,TEXT_Y_OFFSET:+999\n")
    geo_file.write("470     +0.00   +0.00  1       SIZE:13.5,ROTATE:30,WIDTH:0.01,EDGE_SHIFT:0.6,X_CROP:0.3575,Y_CROP:0.5875,INVERT:1,X_CROP:0.545,INVERT:1,ROTATE:180,TEXT_Y_OFFSET:+999\n")
    geo_file.write("471     +0.00   +0.00  1       SIZE:13.5,ROTATE:30,WIDTH:0.01,EDGE_SHIFT:0.6,X_CROP:0.6425,Y_CROP:0.5875,INVERT:1,INVERT:1,ROTATE:180,TEXT_Y_OFFSET:+999\n")    
    geo_file.write("472     +0.00   +0.00  1       SIZE:13.5,ROTATE:30,WIDTH:0.01,EDGE_SHIFT:0.6,X_CROP:0.6425,Y_CROP:0.4125,TEXT_Y_OFFSET:+999\n")
    geo_file.write("473     +0.00   +0.00  1       SIZE:13.5,ROTATE:30,WIDTH:0.01,EDGE_SHIFT:0.6,X_CROP:0.3575,Y_CROP:0.4125,INVERT:1,X_CROP:0.556,TEXT_Y_OFFSET:+999\n")
    geo_file.write("474     +0.00   +0.00  1       SIZE:13.5,ROTATE:30,WIDTH:0.01,EDGE_SHIFT:0.6,X_CROP:0.6425,Y_CROP:0.4125,INVERT:1,TEXT_Y_OFFSET:+999\n")
    
    #test structures
    geo_file.write("475     +5.50   -3.90   12      SIZE:0.5\n")
    geo_file.write("476     +5.50   -4.33   12      SIZE:0.5\n")
    geo_file.write("477     +5.50   -4.76   12      SIZE:0.5\n")
    geo_file.write("478     +5.50   -5.20   12      SIZE:0.5\n")
    geo_file.write("479     +6.00   -3.90   12      SIZE:0.5\n")
    geo_file.write("480     +6.00   -4.33   12      SIZE:0.5\n")
    geo_file.write("481     +6.00   -4.76   12      SIZE:0.5\n")
    geo_file.write("482     +6.00   -5.20   12      SIZE:0.5\n")


#hexplot plotting
HEXPLOTCOMMAND = "<HEXPLOTDIR>/bin/HexPlot \
	    -g <GEOFILE> \
	    -o <OUTPUTFILE> \
	    -p GEO --pn 0".replace("<HEXPLOTDIR>", os.environ["HEXPLOT_DIR"]).replace("<GEOFILE>", geo_file_path).replace("<OUTPUTFILE>", geo_file_path.replace(".txt", ".pdf"))


env = {}
env.update(os.environ)
p = Popen(HEXPLOTCOMMAND, stdout=sys.stdout, stderr=sys.stderr, shell=True, env=env)
out, err = p.communicate()
code = p.returncode

print("To be implemented")
print(np.unique(missing_types))