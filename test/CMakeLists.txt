# Configuration file testing

#================================
#            Basic tests        #
#================================
OPTION(DO_BASIC_TESTS "Perform basic tests?" ON)
IF(DO_BASIC_TESTS)
	MESSAGE(STATUS "Unit tests: basic tests")

	ADD_TEST(NAME run_test COMMAND ${PROJECT_SOURCE_DIR}/bin/${PROJECT_NAME})

	ADD_TEST(NAME help_test COMMAND ${PROJECT_SOURCE_DIR}/bin/${PROJECT_NAME} --help)

	ADD_TEST(NAME defaults_test COMMAND ${PROJECT_SOURCE_DIR}/bin/${PROJECT_NAME} --defaults)
	SET_TESTS_PROPERTIES(defaults_test PROPERTIES PASS_REGULAR_EXPRESSION "Arguments set to")

	ADD_TEST(NAME examples_test COMMAND ${PROJECT_SOURCE_DIR}/bin/${PROJECT_NAME} --examples)
	SET_TESTS_PROPERTIES(examples_test PROPERTIES PASS_REGULAR_EXPRESSION "Example commands can be executed via")
ENDIF(DO_BASIC_TESTS)

#================================
#         Example tests         #
#================================
OPTION(DO_EXAMPLE_TESTS "Perform example tests?" ON)
IF(DO_EXAMPLE_TESTS)
	FILE(GLOB TEST_LIST_EXAMPLES "${CMAKE_CURRENT_SOURCE_DIR}/test_files/example_*.txt")
	LIST(LENGTH TEST_LIST_EXAMPLES NUM_TEST_EXAMPLES)
	MESSAGE(STATUS "Unit tests: ${NUM_TEST_EXAMPLES} example functionality tests")

	FOREACH(TEST ${TEST_LIST_EXAMPLES})
		STRING(REGEX MATCH "_([0-9]+).txt" IDX ${TEST})
		SET(IDX "${CMAKE_MATCH_1}")
		STRING(REGEX MATCH "([A-Za-z0-9_]+).txt" TEST_NAME ${TEST})
		SET(TEST_NAME "${CMAKE_MATCH_1}")
		#MESSAGE(STATUS "Unit tests: adding example ${IDX} from string ${TEST}")

	    FILE(STRINGS ${TEST} EXPRESSIONS REGEX "^[^#]")
	    SET(VAR 0)
	    FOREACH(LINE ${EXPRESSIONS})
			#MESSAGE(STATUS "Unit test ${TEST_NAME}_${VAR}: looking for expressions ${LINE}")
			SET(TEST_NAME_REAL "${TEST_NAME}_${VAR}")

			ADD_TEST(
	    		NAME ${TEST_NAME_REAL}
	        	COMMAND ${PROJECT_SOURCE_DIR}/bin/${PROJECT_NAME} --yestoall --testinfo --example ${IDX}
	    	)
	    	
	    	SET_TESTS_PROPERTIES(${TEST_NAME_REAL} PROPERTIES PASS_REGULAR_EXPRESSION "${LINE}")
		    MATH(EXPR VAR "${VAR}+1")
	    ENDFOREACH()
    ENDFOREACH()
ENDIF(DO_EXAMPLE_TESTS)

#================================
#         Geometry tests         #
#================================
OPTION(DO_GEOMETRY_TESTS "Perform geometry tests?" ON)
IF(DO_GEOMETRY_TESTS)
	FILE(GLOB TEST_LIST_GEOS "${CMAKE_CURRENT_SOURCE_DIR}/test_files/geo_test_*.txt")
	LIST(LENGTH TEST_LIST_GEOS NUM_TEST_EXAMPLES)
	MESSAGE(STATUS "Unit tests: ${NUM_TEST_EXAMPLES} geometry functionality tests")

	FOREACH(TEST ${TEST_LIST_GEOS})
	    FILE(STRINGS ${TEST} GEO REGEX "^#")
		STRING(REGEX MATCH "[A-Za-z0-9_]+.txt" GEO ${GEO})
		STRING(REGEX MATCH "[A-Za-z0-9_]+.txt" TEST_NAME ${TEST})
		STRING(REPLACE ".txt" "" TEST_NAME "${TEST_NAME}")
		#MESSAGE(STATUS "Unit tests: adding test for ${GEO}")

	    FILE(STRINGS ${TEST} EXPRESSIONS REGEX "^[^#]")
	    SET(VAR 0)
	    FOREACH(LINE ${EXPRESSIONS})
			#MESSAGE(STATUS "Unit test ${TEST_NAME}_${VAR}: looking for expressions ${LINE}")
			SET(TEST_NAME_REAL "${TEST_NAME}_${VAR}")

			ADD_TEST(
	    		NAME ${TEST_NAME_REAL}
	            COMMAND ${PROJECT_SOURCE_DIR}/bin/${PROJECT_NAME} --yestoall --testinfo --example 1 -g  ${PROJECT_SOURCE_DIR}/geo/${GEO}
	    	)
	    	
	    	SET_TESTS_PROPERTIES(${TEST_NAME_REAL} PROPERTIES PASS_REGULAR_EXPRESSION "${LINE}")
		    MATH(EXPR VAR "${VAR}+1")
	    ENDFOREACH()

    ENDFOREACH()
ENDIF(DO_GEOMETRY_TESTS)

#================================
#           Data tests          #
#================================
OPTION(DO_DATA_TESTS "Perform data tests?" ON)
IF(DO_DATA_TESTS)
	FILE(GLOB TEST_LIST_DATAS "${CMAKE_CURRENT_SOURCE_DIR}/test_files/data_test_*.txt")
	LIST(LENGTH TEST_LIST_DATAS NUM_TEST_EXAMPLES)
	MESSAGE(STATUS "Unit tests: ${NUM_TEST_EXAMPLES} data functionality tests")

	FOREACH(TEST ${TEST_LIST_DATAS})
	    FILE(STRINGS ${TEST} DATA REGEX "^#")
		STRING(REGEX MATCH "[A-Za-z0-9_]+.txt" DATA ${DATA})
		STRING(REGEX MATCH "[A-Za-z0-9_]+.txt" TEST_NAME ${TEST})
		STRING(REPLACE ".txt" "" TEST_NAME "${TEST_NAME}")
		#MESSAGE(STATUS "Unit tests: adding test for ${DATA}")
	    
	    FILE(STRINGS ${TEST} EXPRESSIONS REGEX "^[^#]")
	    SET(VAR 0)
	    FOREACH(LINE ${EXPRESSIONS})
			#MESSAGE(STATUS "Unit test ${TEST_NAME}_${VAR}: looking for expressions ${LINE}")
			SET(TEST_NAME_REAL "${TEST_NAME}_${VAR}")

			ADD_TEST(
	    		NAME ${TEST_NAME_REAL}
	        	COMMAND ${PROJECT_SOURCE_DIR}/bin/${PROJECT_NAME} --yestoall --testinfo --example 2 -i ${PROJECT_SOURCE_DIR}/examples/${DATA}
	    	)
	    	
	    	SET_TESTS_PROPERTIES(${TEST_NAME_REAL} PROPERTIES PASS_REGULAR_EXPRESSION "${LINE}")
		    MATH(EXPR VAR "${VAR}+1")
	    ENDFOREACH()

    ENDFOREACH()
ENDIF(DO_DATA_TESTS)
