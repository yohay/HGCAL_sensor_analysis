typeID,area[um2],perimeter[um],description
n,55508574,27718,normal cell
o,12781006,12673,calibration cell
O,42086052,40705,outer calibration cell
C,45972330,26192,large edge corner cell
c,35021256,23243,small edge corner cell
v,12765907,16697,(very small) corner cell
E,54114838,28429,large edge cell
e,35496352,23777,small edge cell
G,50145773,28968,large edge cell next to GR contact
g,31527280,24317,small edge cell next to GR contact
